package course_proj.ci.tiseddev.com.courseproj.interfaces;

import android.graphics.Bitmap;

/**
 * Created by dmytro_vodnik on 11/30/15.
 */
public interface ImageProcessingInterface {

    void processImage(Bitmap src);
}

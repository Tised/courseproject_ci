package course_proj.ci.tiseddev.com.courseproj.activities;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import course_proj.ci.tiseddev.com.courseproj.R;
import course_proj.ci.tiseddev.com.courseproj.interfaces.ImageProcessingInterface;
import course_proj.ci.tiseddev.com.courseproj.models.Item;

public class LauncherActivity extends AppCompatActivity implements ImageProcessingInterface {

    ArrayList<String> str = new ArrayList<String>();
    private Boolean firstLvl = true;
    private static final String TAG = "F_PATH";
    private TextView tvSetResult;
    private ImageView imgPreview;
    private Uri fileUri;
    private Item[] fileList;
    private File path = new File(Environment.getExternalStorageDirectory() + ""); private String chosenFile;
    private static final int DIALOG_LOAD_FILE = 1000;
    private int totalCirclesDetected = 0;
    ListAdapter adapter;
    TextView loadHint;
    Button btnAnalyze;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.launcher_activity);

        imgPreview = (ImageView) findViewById(R.id.imgPreview);
        tvSetResult = (TextView) findViewById(R.id.tvSetResult);
        loadHint = (TextView) findViewById(R.id.load_hint);
        btnAnalyze = (Button) findViewById(R.id.btnCapturePicture);

        if (!OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_2, this,
                mOpenCVCallBack)) {

            Log.e("TEST", "Cannot connect to OpenCV Manager");
        }
    }


    private BaseLoaderCallback mOpenCVCallBack = new BaseLoaderCallback(this) {

        @Override
        public void onManagerConnected(int status) {

            switch (status) {

                case LoaderCallbackInterface.SUCCESS: { } break;

                default: {

                    super.onManagerConnected(status);
                }

                break;
            }
        }
    };


    private void loadFileList() {

        try {

            path.mkdirs();
        } catch (SecurityException e) {

            Log.e(TAG, "unable to write on the sd card ");
        }

        if (path.exists()) {

            FilenameFilter filter = (dir, filename) -> {

                File sel = new File(dir, filename);

                return (sel.isFile() || sel.isDirectory()) && !sel.isHidden();
            };

            String[] fList = path.list(filter);
            fileList = new Item[fList.length];

            for (int i = 0; i < fList.length; i++) {

                fileList[i] = new Item(fList[i], R.drawable.file);
                File sel = new File(path, fList[i]);

                if (sel.isDirectory()) {

                    fileList[i].icon = R.drawable.folder;
                    Log.d("DIRECTORY", fileList[i].file);
                }
            }

            if (!firstLvl) {

                Item temp[] = new Item[fileList.length + 1];

                for (int i = 0; i < fileList.length; i++) {

                    temp[i + 1] = fileList[i];
                }

                temp[0] = new Item("Up", R.drawable.upfolder);
                fileList = temp;
            }
        }

        adapter = new ArrayAdapter<Item>(this,android.R.layout.select_dialog_item, android.R.id.text1,fileList) {

            public View getView(int position, View convertView, ViewGroup parent) {

                View view = super.getView(position, convertView, parent);
                TextView textView = (TextView) view.findViewById(android.R.id.text1);
                textView.setCompoundDrawablesWithIntrinsicBounds( fileList[position].icon, 0, 0, 0);
                int dp5 = (int) (5 * getResources().getDisplayMetrics().density + 0.5f);
                textView.setCompoundDrawablePadding(dp5);

                return view;
            }
        };
    }

    @Override
    protected Dialog onCreateDialog(int id) {

        Dialog dialog = null;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        if (fileList == null) {

            Log.e(TAG, "No files loaded"); dialog = builder.create();
            return dialog;
        }

        switch (id) {
            case DIALOG_LOAD_FILE:
                builder.setTitle("Choose your file");
                builder.setAdapter(adapter, (dialog1, which) -> {

                    chosenFile = fileList[which].file;
                    File sel = new File(path + "/" + chosenFile);
                    fileUri = Uri.parse(sel.toString());

                    if (sel.isDirectory()) {
                        firstLvl = false;
                        str.add(chosenFile);
                        fileList = null;

                        path = new File(sel + "");

                        loadFileList();

                        removeDialog(DIALOG_LOAD_FILE);
                        showDialog(DIALOG_LOAD_FILE);

                        Log.d(TAG, path.getAbsolutePath());
                    }
                    else if (chosenFile.equalsIgnoreCase("up") && !sel.exists()) {

                        String s = str.remove(str.size() - 1);
                        path = new File(path.toString().substring(0,path.toString().lastIndexOf(s)));
                        fileList = null;

                        if (str.isEmpty()) {

                            firstLvl = true;
                        }

                        loadFileList();

                        removeDialog(DIALOG_LOAD_FILE);
                        showDialog(DIALOG_LOAD_FILE);
                        Log.d(TAG, path.getAbsolutePath());
                    }
                    else {

                        BitmapFactory.Options options = new BitmapFactory.Options();
                        options.inSampleSize = 1;
                        Bitmap bitmap = BitmapFactory.decodeFile(fileUri.getPath(), options);

                        tvSetResult.setVisibility(View.VISIBLE);
                        btnAnalyze.setVisibility(View.VISIBLE);

                        setPreviewImage(bitmap);
                    }
                });
                break;
        }
        dialog = builder.show();

        return dialog;
    }

    public void btnCapturePicture_Click(View v){

        BitmapFactory.Options options = new BitmapFactory.Options();
        Bitmap bitmap = BitmapFactory.decodeFile(fileUri.getPath(), options);

        processImage(bitmap);
    }

    public void loadFile(View v) {

        loadFileList();

        showDialog(DIALOG_LOAD_FILE);
    }

    private void setPreviewImage(Bitmap bitmap) {

        try {

            loadHint.setVisibility(View.GONE);
            imgPreview.setVisibility(View.VISIBLE);
            imgPreview.setImageBitmap(bitmap);
        } catch (NullPointerException e) {

            e.printStackTrace();
        }
    }

    double dist = 6.5;

    @Override
    public void processImage(Bitmap bitmap) {

        SweetAlertDialog progress = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);

        progress.setTitleText(getResources().getString(R.string.attention))
                .setContentText(getResources().getString(R.string.processing))
                .setConfirmText(getResources().getString(R.string.OK));

        progress.setCancelable(false);

        progress.show();

        new Thread(() -> {

            Bitmap bmOut = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), bitmap.getConfig());

            Mat tmp = new Mat();
            Mat newImg = new Mat();
            Mat distImg = new Mat();

            Utils.bitmapToMat(bitmap, tmp);

            Imgproc.cvtColor(tmp, tmp, Imgproc.COLOR_BGR2GRAY);
            Imgproc.GaussianBlur(tmp, tmp, new Size(3, 3), 0);

            Mat kernel = Imgproc.getStructuringElement(Imgproc.MORPH_ELLIPSE, new Size(5,5));

            Imgproc.morphologyEx(tmp, newImg, Imgproc.MORPH_CLOSE, kernel);
            Imgproc.threshold(newImg, newImg, 255, 255, Imgproc.THRESH_BINARY_INV | Imgproc.THRESH_OTSU);
            Imgproc.adaptiveThreshold(tmp, newImg, 255, Imgproc.ADAPTIVE_THRESH_MEAN_C, Imgproc.THRESH_BINARY_INV, 5, 4);
            Imgproc.Canny(newImg, newImg, 80, 90);

            List<MatOfPoint> contours = new ArrayList<MatOfPoint>();

            Imgproc.findContours(newImg, contours, new Mat(), Imgproc.RETR_LIST,Imgproc.CHAIN_APPROX_SIMPLE);
            Imgproc.drawContours(tmp, contours, -1, new Scalar(0, 0,255), 1);

            Mat mIntermediateMat = new Mat();

            int iCannyUpperThreshold = 100;
            int iMinRadius = 0;
            int iMaxRadius = 0;
            int radius = 0;

            Imgproc.HoughCircles(newImg, mIntermediateMat, Imgproc.CV_HOUGH_GRADIENT, 2, newImg.rows() / dist);//, iCannyUpperThreshold, 100, iMinRadius, iMaxRadius);

            Utils.matToBitmap(tmp, bmOut);

            runOnUiThread(() -> {

                setPreviewImage(bmOut);
                tvSetResult.setText(getResources().getString(R.string.amount_cells) + mIntermediateMat.cols());

                progress.cancel();
            });

        }).start();
    }
}

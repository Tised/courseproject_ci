package course_proj.ci.tiseddev.com.courseproj.models;

/**
 * Created by dmytro_vodnik on 11/30/15.
 */
public class Item {

    public String file;
    public int icon;

    public Item (String file, Integer icon) {

        this.file = file;
        this.icon = icon;
    }

    @Override
    public String toString() {

        return file;
    }
}

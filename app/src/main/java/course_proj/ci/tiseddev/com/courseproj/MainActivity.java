package course_proj.ci.tiseddev.com.courseproj;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;

import course_proj.ci.tiseddev.com.courseproj.models.Item;

public class MainActivity extends AppCompatActivity {

    ArrayList<String> str = new ArrayList<String>();
    private Boolean firstLvl = true;
    private static final String TAG = "F_PATH";
    private TextView tvSetResult;
    private Button btnCapturePicture;
    private ImageView imgPreview;
    private Uri fileUri;
    private Item[] fileList;
    private File path = new File(Environment.getExternalStorageDirectory() + ""); private String chosenFile;
    private static final int DIALOG_LOAD_FILE = 1000;
    private int totalCirclesDetected = 0;
    ListAdapter adapter;

    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.photo_preview);
        imgPreview = (ImageView) findViewById(R.id.imgPreview);
        btnCapturePicture = (Button) findViewById(R.id.btnCapturePicture);
        tvSetResult = (TextView) findViewById(R.id.tvSetResult);

        if (!OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_2, this,
                mOpenCVCallBack)) {

            Log.e("TEST", "Cannot connect to OpenCV Manager");
        }
    }

    private BaseLoaderCallback mOpenCVCallBack = new BaseLoaderCallback(this) {

        @Override
        public void onManagerConnected(int status) {

            switch (status) {

                case LoaderCallbackInterface.SUCCESS: { } break;

                default: {

                    super.onManagerConnected(status);
                }

                break;
            }
        }
    };


    private void loadFileList() {

        try {

            path.mkdirs();
        } catch (SecurityException e) {

            Log.e(TAG, "unable to write on the sd card ");
        }

        if (path.exists()) {

            FilenameFilter filter = new FilenameFilter() {

                @Override
                public boolean accept(File dir, String filename) {

                    File sel = new File(dir, filename);

                    return (sel.isFile() || sel.isDirectory()) && !sel.isHidden();
                }
            };

            String[] fList = path.list(filter);
            fileList = new Item[fList.length];

            for (int i = 0; i < fList.length; i++) {

                fileList[i] = new Item(fList[i], R.mipmap.ic_launcher);
                File sel = new File(path, fList[i]);

                if (sel.isDirectory()) {

                    fileList[i].icon = R.mipmap.ic_launcher;
                    Log.d("DIRECTORY", fileList[i].file);
                }
            }

            if (!firstLvl) {

                Item temp[] = new Item[fileList.length + 1];

                for (int i = 0; i < fileList.length; i++) {

                    temp[i + 1] = fileList[i];
                }

                temp[0] = new Item("Up", R.mipmap.ic_launcher);
                fileList = temp;
            }
        }

        adapter = new ArrayAdapter<Item>(this,android.R.layout.select_dialog_item, android.R.id.text1,fileList) {

            public View getView(int position, View convertView, ViewGroup parent) {

            View view = super.getView(position, convertView, parent);
            TextView textView = (TextView) view.findViewById(android.R.id.text1);
            textView.setCompoundDrawablesWithIntrinsicBounds( fileList[position].icon, 0, 0, 0);
            int dp5 = (int) (5 * getResources().getDisplayMetrics().density + 0.5f);
            textView.setCompoundDrawablePadding(dp5);

                return view;
            }
        };
    }

    @Override
    protected Dialog onCreateDialog(int id) {

        Dialog dialog = null;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        if (fileList == null) {

            Log.e(TAG, "No files loaded"); dialog = builder.create();
            return dialog;
        }

        switch (id) {
            case DIALOG_LOAD_FILE:
                builder.setTitle("Choose your file");
                builder.setAdapter(adapter, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {

                        chosenFile = fileList[which].file;
                        File sel = new File(path + "/" + chosenFile);
                        fileUri = Uri.parse(sel.toString());

                        if (sel.isDirectory()) {
                            firstLvl = false;
                            str.add(chosenFile);
                            fileList = null;

                            path = new File(sel + "");

                            loadFileList();

                            removeDialog(DIALOG_LOAD_FILE);
                            showDialog(DIALOG_LOAD_FILE);

                            Log.d(TAG, path.getAbsolutePath());
                        }
                        else if (chosenFile.equalsIgnoreCase("up") && !sel.exists()) {

                            String s = str.remove(str.size() - 1);
                            path = new File(path.toString().substring(0,path.toString().lastIndexOf(s)));
                            fileList = null;

                            if (str.isEmpty()) {

                                firstLvl = true;
                            }

                            loadFileList();

                            removeDialog(DIALOG_LOAD_FILE);
                            showDialog(DIALOG_LOAD_FILE);
                            Log.d(TAG, path.getAbsolutePath());
                        }
                        else {

                            BitmapFactory.Options options = new BitmapFactory.Options();
                            options.inSampleSize = 1;
                            Bitmap bitmap = BitmapFactory.decodeFile(fileUri.getPath(), options);
                            setPreviewImage(bitmap);
                        }
                    }
                });
                break;
        }
        dialog = builder.show();

        return dialog;
    }

    public void btnCapturePicture_Click(View v){

        setPreviewImage(conturCanny());

        contourImg();
        tvSetResult.setText("Amount of blood cells: " + totalCirclesDetected);
    }

    public void loadFile(View v) {

        loadFileList();

        showDialog(DIALOG_LOAD_FILE);
    }

    private void setPreviewImage(Bitmap bitmap) {

        try {

            imgPreview.setVisibility(View.VISIBLE);
            imgPreview.setImageBitmap(bitmap);
        } catch (NullPointerException e) {

            e.printStackTrace();
        }
    }

    double dist = 6.5;

    public Bitmap conturCanny(){

        totalCirclesDetected = 0;

        BitmapFactory.Options options = new BitmapFactory.Options();
        Bitmap bitmap = BitmapFactory.decodeFile(fileUri.getPath(), options);
        Bitmap bmOut = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), bitmap.getConfig());

        Mat tmp = new Mat();
        Mat newImg = new Mat();
        Mat distImg = new Mat();

        Utils.bitmapToMat(bitmap, tmp);

        Imgproc.cvtColor(tmp, tmp, Imgproc.COLOR_BGR2GRAY);
        Imgproc.GaussianBlur(tmp, tmp, new Size(3, 3), 0);
        Mat kernel = Imgproc.getStructuringElement(Imgproc.MORPH_ELLIPSE, new Size(5,5));

        Imgproc.morphologyEx(tmp, newImg, Imgproc.MORPH_CLOSE, kernel);
        Imgproc.threshold(newImg, newImg, 255, 255, Imgproc.THRESH_BINARY_INV | Imgproc.THRESH_OTSU);
        Imgproc.adaptiveThreshold(tmp, newImg, 255, Imgproc.ADAPTIVE_THRESH_MEAN_C, Imgproc.THRESH_BINARY_INV, 5, 4);
        Imgproc.Canny(newImg, newImg, 80, 90);

        List<MatOfPoint> contours = new ArrayList<MatOfPoint>();

        Imgproc.findContours(newImg, contours, new Mat(), Imgproc.RETR_LIST,Imgproc.CHAIN_APPROX_SIMPLE);
        Imgproc.drawContours(tmp, contours, -1, new Scalar(0, 0,255), 1);

        Mat mIntermediateMat = new Mat();

        int iCannyUpperThreshold = 100;
        int iMinRadius = 0;
        int iMaxRadius = 0;
        int radius = 0;

        Imgproc.HoughCircles(newImg, mIntermediateMat, Imgproc.CV_HOUGH_GRADIENT, 2, newImg.rows() / dist);//, iCannyUpperThreshold, 100, iMinRadius, iMaxRadius);

        Log.d(TAG, "nasty c === " + mIntermediateMat.cols() );

        if (mIntermediateMat.cols() > 0) {

            for (int x = 0; x < mIntermediateMat.cols(); x++) {

                double vCircle[] = mIntermediateMat.get(0, x);

                if (vCircle == null)
                    break;

                totalCirclesDetected++;

                Point pt = new Point();

                pt.x = Math.round(vCircle[0]);
                pt.y = Math.round(vCircle[1]);
                radius = (int) Math.round(vCircle[2]);

               // Core.circle(tmp, pt, radius, new Scalar(0,255,0), 1);
            }
        }

        Utils.matToBitmap(tmp, bmOut);

        return bmOut;
    }

    private Bitmap contourImg() {

        String imgPath = fileUri.getPath();

        BitmapFactory.Options options = new BitmapFactory.Options();
        Bitmap i =  BitmapFactory.decodeFile(fileUri.getPath(), options);
        Bitmap bmpImg = i.copy(Bitmap.Config.ARGB_8888, false);
        //Log.i("after Bitmap bmpImg",""+imgPath);
        Mat srcMat = new Mat ( bmpImg.getHeight(), bmpImg.getWidth(), CvType.CV_8UC3);
        Bitmap myBitmap32 = bmpImg.copy(Bitmap.Config.ARGB_8888, true);
        Utils.bitmapToMat(bmpImg, srcMat);


        //convert to gray scale and save image
        Mat gray = new Mat(srcMat.size(), CvType.CV_8UC1);

        Imgproc.cvtColor(srcMat, gray, Imgproc.COLOR_BGRA2GRAY);
        //write bitmap
        Boolean grayBool = Highgui.imwrite(imgPath + "gray.jpg", gray);
        //Toast.makeText(this, "Gray scale image saved!", Toast.LENGTH_SHORT).show();

        //thresholding
        Mat threshed = new Mat(bmpImg.getWidth(),bmpImg.getHeight(), CvType.CV_8UC1);
        Imgproc.adaptiveThreshold(gray, threshed, 255, Imgproc.ADAPTIVE_THRESH_MEAN_C, Imgproc.THRESH_BINARY, 75, 5);//15, 8 were original tests. Casey was 75,10
        //Imgproc.adaptiveThreshold(threshed, threshed, 255, Imgproc.ADAPTIVE_THRESH_MEAN_C, Imgproc.THRESH_BINARY_INV, 75, 5);
        Core.bitwise_not(threshed, threshed);
        Utils.matToBitmap(threshed, bmpImg);


        //write bitmap
        Boolean boolThr = Highgui.imwrite(imgPath + "threshed.jpg", threshed);
        //Toast.makeText(this, "Thresholded image saved!", Toast.LENGTH_SHORT).show();


        //perform Canny Edge Detection and convert to 4 channel
        Mat edge = new Mat();
        Mat dst = new Mat();
        Imgproc.Canny(threshed, edge, 80, 90);
        Imgproc.cvtColor(edge, dst, Imgproc.COLOR_GRAY2RGBA,4);
        //convert to bitmap
        Bitmap resultBitmap = Bitmap.createBitmap(dst.cols(), dst.rows(),Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(dst, resultBitmap);
        //write bitmap
        Boolean bool = Highgui.imwrite(imgPath + "edges.jpg", dst);
        //Toast.makeText(this, "Edge information image saved!", Toast.LENGTH_SHORT).show();


        //smoothing
        Mat smoothed = new Mat(bmpImg.getWidth(),bmpImg.getHeight(), CvType.CV_8UC1);
        Imgproc.GaussianBlur(dst, smoothed, new org.opencv.core.Size(3,3), 50);
        Utils.matToBitmap(smoothed, bmpImg);
        //write bitmap
        Boolean boolSmoothed = Highgui.imwrite(imgPath + "smoothed.jpg", smoothed);
        //Toast.makeText(this, "Smoothed image saved!", Toast.LENGTH_SHORT).show();

        //morphological operations
        //dilation
        Mat dilated = new Mat(bmpImg.getWidth(),bmpImg.getHeight(), CvType.CV_8UC1);
        Imgproc.dilate(smoothed, dilated, Imgproc.getStructuringElement(Imgproc.MORPH_ELLIPSE, new org.opencv.core.Size (16, 16)));
        Utils.matToBitmap(dilated, bmpImg);
        //write bitmap
        Boolean boolDilated = Highgui.imwrite(imgPath + "dilated.jpg", dilated);
        //Toast.makeText(this, "Dilated image saved!", Toast.LENGTH_SHORT).show();

        //erosion
        Mat eroded = new Mat(bmpImg.getWidth(),bmpImg.getHeight(), CvType.CV_8UC1);

        Imgproc.erode(dilated, eroded, Imgproc.getStructuringElement(Imgproc.MORPH_ELLIPSE, new org.opencv.core.Size(15, 15)));
        Utils.matToBitmap(eroded, bmpImg);

        //write bitmap
        Boolean boolEroded = Highgui.imwrite(imgPath + "eroded.jpg", eroded);
        //Toast.makeText(this, "Eroded image saved!", Toast.LENGTH_SHORT).show();

        //hough circles
        Mat circles = new Mat();

        // parameters
        int iCannyUpperThreshold = 100;
        int iMinRadius = 0;
        int iMaxRadius = 0;
        int radius = 0;


        Mat toFind = new Mat(eroded.size(), CvType.CV_8SC1);
        Imgproc.cvtColor(eroded, toFind, Imgproc.COLOR_BGR2GRAY, 0);

        Utils.matToBitmap(toFind, bmpImg);

        Imgproc.HoughCircles(toFind, circles, Imgproc.CV_HOUGH_GRADIENT, 2, srcMat.rows() / dist);//, iCannyUpperThreshold, 100, iMinRadius, iMaxRadius);

        //   Imgproc.HoughCircles(toFind,circles,Imgproc.CV_HOUGH_GRADIENT,1.0, eroded.rows()/8,iCannyUpperThreshold,iAccumulator,iMinRadius,iMaxRadius);
        Log.i("cccccccccccccccccccccc", "cccc" + circles.cols());

        // draw
        if (circles.cols() > 0) {

            Toast.makeText(this, "Coins : " +circles.cols() , Toast.LENGTH_LONG).show();
//            alertString = "Number of coins detected : " + circles.cols();
//            displayAlert(alertString);
        } else {

            Toast.makeText(this, "No coins found", Toast.LENGTH_LONG).show();
//            alertString = "No objects detected";
//            displayAlert(alertString);
        }

        List<MatOfPoint> contours = new ArrayList<MatOfPoint>();

        Imgproc.findContours(toFind, contours, new Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);
       // Imgproc.drawContours(tmp, contours, -1, new Scalar(0, 0,255), 1);

        if (circles.cols() > 0) {

            for (int x = 0; x < circles.cols(); x++) {

                double vCircle[] = circles.get(0, x);

                if (vCircle == null)
                    break;

                totalCirclesDetected++;

                Point pt = new Point();

                pt.x = Math.round(vCircle[0]);
                pt.y = Math.round(vCircle[1]);
                radius = (int) Math.round(vCircle[2]);

                Core.circle(toFind, pt, radius, new Scalar(0,255,0), 1);
            }
        }

        return bmpImg;
    }
}

